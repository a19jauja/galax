#ifdef GALAX_MODEL_CPU_FAST

#include <cmath>
#include <iostream>
#include "Model_CPU_fast.hpp"
#include <vector>
#include <xsimd/xsimd.hpp>
#include <omp.h>

namespace xs = xsimd;
using b_type = xs::batch<float, xs::avx2>;
using uib_type = xs::batch<unsigned, xs::avx2>;

struct Rot
{
    static constexpr unsigned get(unsigned i, unsigned n)
    {
        return (i + n - 1) % n;
    }
};


Model_CPU_fast
::Model_CPU_fast(const Initstate& initstate, Particles& particles)
: Model_CPU(initstate, particles)
{
}

void Model_CPU_fast
::step()
{
    std::fill(accelerationsx.begin(), accelerationsx.end(), 0);
    std::fill(accelerationsy.begin(), accelerationsy.end(), 0);
    std::fill(accelerationsz.begin(), accelerationsz.end(), 0);

// OMP + xsimd version
    constexpr std::size_t inc = b_type::size;
    std::size_t vec_size = n_particles - n_particles % inc;
    float range_inc[inc];
    for (int k = 0; k < inc; ++k){
        range_inc[k] = k;
    }
    const b_type inc_i = b_type::load_unaligned(range_inc);
    const b_type tenth = 0.1f;
    const b_type one = 1.0;
    const b_type two = 2.0f;
    const b_type three = 3.0;
    const b_type ten = 10.0;

#pragma omp parallel for
    for (int i = 0; i < vec_size; i += inc) {
        // Load body i
        const b_type rposx_i = b_type::load_unaligned(&particles.x[i]);
        const b_type rposy_i = b_type::load_unaligned(&particles.y[i]);
        const b_type rposz_i = b_type::load_unaligned(&particles.z[i]);
              b_type raccx_i = b_type::load_unaligned(&accelerationsx[i]);
              b_type raccy_i = b_type::load_unaligned(&accelerationsy[i]);
              b_type raccz_i = b_type::load_unaligned(&accelerationsz[i]);

        for (int j = 0; j < vec_size; j += inc) {
            b_type rposx_j = b_type::load_unaligned(&particles.x[j]);
            b_type rposy_j = b_type::load_unaligned(&particles.y[j]);
            b_type rposz_j = b_type::load_unaligned(&particles.z[j]);

            b_type imasses_j = b_type::load_unaligned(&initstate.masses[j]);

            b_type inc_j = inc_i;

            for (int k = 0; k < inc; k++) {
                const b_type diffx = rposx_j - rposx_i;
                const b_type diffy = rposy_j - rposy_i;
                const b_type diffz = rposz_j - rposz_i;

                b_type dij = diffx * diffx + diffy * diffy + diffz * diffz;

                // dij = xs::select(dij < one, ten, ten / (xs::sqrt(dij)*xs::sqrt(dij)*xs::sqrt(dij))); // Précision parfaite
                // dij = xs::select(dij < one, ten, ten / (xs::sqrt(dij)*dij)); // Moins précis
                // dij = xs::select(dij < one, ten, ten / xs::sqrt(dij*dij*dij)); // Moins précis
                // dij = xs::select(dij < one, ten, ten / xs::pow(xs::sqrt(dij),three)); // Peu précis
                dij = xs::select(dij < one, ten, ten * xs::rsqrt(dij*dij*dij)); // Plus rapide, moins précis

                if((j+inc) < i || j >= (i + inc)){ // Cas où l'on est sûr que j n'est égal à aucun i
                    raccx_i += diffx * dij * imasses_j;
                    raccy_i += diffy * dij * imasses_j;
                    raccz_i += diffz * dij * imasses_j;		            
                }
                else {
                    b_type diff_ji = (float) (j-i); // On crée un batch de bool qui indique à quel "index" il y a égalité
                    diff_ji = diff_ji + inc_j;                    
                    xs::batch_bool eq_ij = eq(diff_ji, inc_i); // On remplace la valeur de racc seulement si i et j sont différents, i.e. le bool associé à l'index est faux     
                    raccx_i = xs::select(eq_ij, raccx_i, raccx_i + diffx * dij * imasses_j);
                    raccy_i = xs::select(eq_ij, raccy_i, raccy_i + diffy * dij * imasses_j);
                    raccz_i = xs::select(eq_ij, raccz_i, raccz_i + diffz * dij * imasses_j);
                }
                rposx_j = xs::swizzle(rposx_j,xs::make_batch_constant<uib_type, Rot>());
                rposy_j = xs::swizzle(rposy_j,xs::make_batch_constant<uib_type, Rot>());
                rposz_j = xs::swizzle(rposz_j,xs::make_batch_constant<uib_type, Rot>());
                
                inc_j = xs::swizzle(inc_j,xs::make_batch_constant<uib_type, Rot>());

                imasses_j = xs::swizzle(imasses_j,xs::make_batch_constant<uib_type, Rot>());
                
            }
        }
        for (int j = vec_size; j < n_particles; ++j) {
            const b_type rposx_j = particles.x[j];
            const b_type rposy_j = particles.y[j];
            const b_type rposz_j = particles.z[j];

            const b_type imasses_j = initstate.masses[j];

            const b_type diffx = rposx_j - rposx_i;
            const b_type diffy = rposy_j - rposy_i;
            const b_type diffz = rposz_j - rposz_i;

            b_type dij = diffx * diffx + diffy * diffy + diffz * diffz;

            // dij = xs::select(dij < one, ten, ten / (xs::sqrt(dij)*xs::sqrt(dij)*xs::sqrt(dij))); // Précision parfaite
            // dij = xs::select(dij < one, ten, ten / (xs::sqrt(dij)*dij)); // Moins précis
            // dij = xs::select(dij < one, ten, ten / xs::sqrt(dij*dij*dij)); // Moins précis
            // dij = xs::select(dij < one, ten, ten / xs::pow(xs::sqrt(dij),three)); // Peu précis
            dij = xs::select(dij < one, ten, ten * xs::rsqrt(dij*dij*dij)); // Plus rapide, moins précis

            if(j < i || j >= (i + inc)){
                raccx_i += diffx * dij * imasses_j;
                raccy_i += diffy * dij * imasses_j;
                raccz_i += diffz * dij * imasses_j;		            
            }
            else {
                b_type diff_ji = (float) (j-i);
                xs::batch_bool eq_ij = eq(diff_ji, inc_i); 
                raccx_i = xs::select(eq_ij, raccx_i, raccx_i + diffx * dij * imasses_j);    
                raccy_i = xs::select(eq_ij, raccy_i, raccy_i + diffy * dij * imasses_j);
                raccz_i = xs::select(eq_ij, raccz_i, raccz_i + diffz * dij * imasses_j);
            }
        }
        raccx_i.store_unaligned(&accelerationsx[i]);
        raccy_i.store_unaligned(&accelerationsy[i]);
        raccz_i.store_unaligned(&accelerationsz[i]);
    }
    
    for(int i = vec_size; i < n_particles; ++i) {
        for(int j = 0; j < n_particles; ++j) {
            if(i != j) {
				const float diffx = particles.x[j] - particles.x[i];
				const float diffy = particles.y[j] - particles.y[i];
				const float diffz = particles.z[j] - particles.z[i];

				float dij = diffx * diffx + diffy * diffy + diffz * diffz;

				if (dij < 1.0) {
					dij = 10.0;
				} else {
					dij = std::sqrt(dij);
					dij = 10.0 / (dij * dij * dij);
				}

				accelerationsx[i] += diffx * dij * initstate.masses[j];
				accelerationsy[i] += diffy * dij * initstate.masses[j];
				accelerationsz[i] += diffz * dij * initstate.masses[j];
			}
        }
    }

#pragma omp barrier
#pragma omp parallel for
    for (int i = 0; i < vec_size; i += inc) {
        b_type rposx_i = b_type::load_unaligned(&particles.x[i]);
        b_type rposy_i = b_type::load_unaligned(&particles.y[i]);
        b_type rposz_i = b_type::load_unaligned(&particles.z[i]);
        b_type rvelx_i = b_type::load_unaligned(&velocitiesx[i]);
        b_type rvely_i = b_type::load_unaligned(&velocitiesy[i]);
        b_type rvelz_i = b_type::load_unaligned(&velocitiesz[i]);
        const b_type raccx_i = b_type::load_unaligned(&accelerationsx[i]);
        const b_type raccy_i = b_type::load_unaligned(&accelerationsy[i]);
        const b_type raccz_i = b_type::load_unaligned(&accelerationsz[i]);

        rvelx_i = xs::fma(raccx_i, two, rvelx_i);
        rvely_i = xs::fma(raccy_i, two, rvely_i);
        rvelz_i = xs::fma(raccz_i, two, rvelz_i);
        rposx_i = xs::fma(rvelx_i, tenth, rposx_i);
        rposy_i = xs::fma(rvely_i, tenth, rposy_i);
        rposz_i = xs::fma(rvelz_i, tenth, rposz_i);

        // rvelx_i = raccx_i * two + rvelx_i;
        // rvely_i = raccy_i * two + rvely_i;
        // rvelz_i = raccz_i * two + rvelz_i;
        // rposx_i = rvelx_i * tenth + rposx_i;
        // rposy_i = rvely_i * tenth + rposy_i;
        // rposz_i = rvelz_i * tenth + rposz_i;

        rvelx_i.store_unaligned(&velocitiesx[i]);
        rvely_i.store_unaligned(&velocitiesy[i]);
        rvelz_i.store_unaligned(&velocitiesz[i]);
        rposx_i.store_unaligned(&particles.x[i]);
        rposy_i.store_unaligned(&particles.y[i]);
        rposz_i.store_unaligned(&particles.z[i]);    
	}

    for(int i = vec_size; i < n_particles; ++i) {
		velocitiesx[i] += accelerationsx[i] * 2.0f;
		velocitiesy[i] += accelerationsy[i] * 2.0f;
		velocitiesz[i] += accelerationsz[i] * 2.0f;
		particles.x[i] += velocitiesx   [i] * 0.1f;
		particles.y[i] += velocitiesy   [i] * 0.1f;
		particles.z[i] += velocitiesz   [i] * 0.1f;
    }
}

#endif // GALAX_MODEL_CPU_FAST
